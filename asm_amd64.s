/*
	func AddUint8x4(addr *uint32, delta *uint32) {
	  a = (*uint8)addr
		b = (*uint8)delta
		*(d+0) = *(a+0) += *(d+0) // capped to 0xFF
		*(d+1) = *(a+1) += *(d+1)
		*(d+2) = *(a+2) += *(d+2)
		*(d+3) = *(a+3) += *(d+3)
	}
*/
/*
	    ONLY DOES AddUint8x1 !
	TEXT ·AddUint8x4(SB),NOSPLIT,$0-16
		MOVQ  delta+8(FP), BP    // BP = args[1]
		MOVB  0(BP), AL          // AL = (uint8)*BP
		MOVQ  addr+0(FP), BP     // BP = args[0]
		LOCK
		XADDB AL, 0(BP)          // *BP += AL; AL = *BP
		JNO enea                 // Goto enea if no overflow
		MOVB $0xFF, AL           // AL = 0xFF
		LOCK
		XCHGB AL, 0(BP)          // *BP = AL
		MOVQ  delta+8(FP), BP    // BP = args[1]
		MOVB $0xFF, 0(BP)        // *BP = 0xFF
		JMP enda                 // goto enda
		enea:
		MOVQ  delta+8(FP), BP    // BP = args[1]
		XADDB AL, 0(BP)          // *BP += AL; AL = *BP
		enda:

		RET
*/

#define ADD_ATOMIC(I, ENEI, ENDI ) \
	MOVB  I(BP), AL \
	MOVQ  addr+0(FP), BP \
	LOCK \
	XADDB AL, I(BP) \
	JNC ENEI \
	MOVB $0xFF, AL \
	LOCK \
	XCHGB AL, I(BP) \
	MOVQ  delta+8(FP), BP \
	MOVB $0xFF, I(BP) \
	JMP ENDI \
	ENEI: \
	MOVQ  delta+8(FP), BP \
	XADDB AL, I(BP) \
	ENDI: \

TEXT ·AddUint8x4(SB),NOSPLIT,$0-16
	MOVQ  delta+8(FP), BP

	ADD_ATOMIC(0,ene0,end0)

	ADD_ATOMIC(1,ene1,end1)

	ADD_ATOMIC(2,ene2,end2)

	ADD_ATOMIC(3,ene3,end3)

	RET
