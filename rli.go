package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"math"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
	"unsafe"

	"bitbucket.org/abex/go-optional"
)

//ConfigT is the Global Config Type
type ConfigT struct {
	Width           int     `default:"1024"          arg:"w" arg:"width"`
	Height          int     `default:"1024"          arg:"h" arg:"height"`
	Seed            int64   `                        arg:"s" arg:"seed"`
	BackgroundColor string  `default:"#000"          arg:"c" arg:"color"`
	SaveFile        string  `default:"out/%s/%i.png" arg:"o" arg:"out"`
	TreeCount       uint    `default:"8"             arg:"t" arg:"trees"`
	SaveInterval    float64 `default:"1.2"           arg:"i" arg:"save-interval"`
	SaveStart       uint64  `default:"100"                   arg:"save-start"`
	StartDiv        float64 `default:"2.5"           arg:"d" arg:"start-diversity"`
	StraightFactor  float64 `default:"7"                     arg:"straight-factor"`
	AgeFactor       float64 `default:"16"            arg:"a" arg:"age-factor"`
	RootColorFactor float64 `defalut:".3"                    arg:"root-color-factor"`
	BigPngs         bool    `default:"true"          arg:"f" arg:"fast-png"`
	LengthFactor    int     `default:"100"           arg:"l" arg:"length-factor"`
	Unrooted        bool    `default:"0"             arg:"u" arg:"unrooted"`
	BgColor         color.RGBA
}

//Config is the global config singleton
var Config ConfigT

//ParseUint8H parses a uint8 from hex string. Panics on failure
func ParseUint8H(s string) uint8 {
	v, err := strconv.ParseUint(s, 16, 8)
	if err != nil {
		panic(err)
	}
	return uint8(v)
}

//Save saves image i to the output dir, with c iterations
func Save(i *image.RGBA, c uint64) {
	n := strings.Replace(strings.Replace(Config.SaveFile, "%i", fmt.Sprintf("%v", c), -1), "%s", fmt.Sprintf("%v", Config.Seed), -1)
	os.MkdirAll(filepath.Dir(n), 0777)
	fi, err := os.OpenFile(n, os.O_CREATE, 0777)
	if err != nil {
		panic(err)
	}
	defer fi.Close()
	enc := png.Encoder{CompressionLevel: png.DefaultCompression}
	if Config.BigPngs {
		enc.CompressionLevel = png.BestSpeed
	}
	err = enc.Encode(fi, i)
	if err != nil {
		panic(err)
	}
}

//Gaussian generates a guassian distribitued random float
func Gaussian(g *rand.Rand) float64 {
	// Box-Muller Transform
	var r, x, y float64
	for r >= 1 || r == 0 {
		x = (g.Float64() * 2) - 1
		y = (g.Float64() * 2) - 1
		r = x*x + y*y
	}
	return x * math.Sqrt(-2*math.Log(r)/r)
}

func main() {
	opt := optional.New(&Config)
	opt.AddSourceFast(0, opt.StructTags("default"))
	opt.AddSource(1, opt.Arguments(os.Args))

	if Config.BackgroundColor[0] != '#' || !(len(Config.BackgroundColor) == 4 || len(Config.BackgroundColor) == 7) {
		panic("Invalid Color")
	}
	if len(Config.BackgroundColor) == 4 { // reform #ABC > #AABBCC
		c := []byte{
			'#',
			Config.BackgroundColor[1],
			Config.BackgroundColor[1],
			Config.BackgroundColor[2],
			Config.BackgroundColor[2],
			Config.BackgroundColor[3],
			Config.BackgroundColor[3],
		}
		Config.BackgroundColor = string(c)
	}

	Config.BgColor = color.RGBA{
		ParseUint8H(Config.BackgroundColor[1:2]),
		ParseUint8H(Config.BackgroundColor[2:4]),
		ParseUint8H(Config.BackgroundColor[4:6]),
		255,
	}

	if Config.Seed == 0 {
		Config.Seed = time.Now().UnixNano()
	}
	rand.Seed(Config.Seed)

	bound := image.Rect(0, 0, Config.Width, Config.Height)
	i := image.NewRGBA(bound)
	draw.Draw(i, bound, &image.Uniform{Config.BgColor}, image.ZP, draw.Src) //draw bg

	//Oh boy
	pix := uintptr((unsafe.Pointer)(&i.Pix[0]))

	Add := func(x, y uintptr, add []int8) {
		v := pix + (y*uintptr(i.Stride) + (x * 4)) //get addr of pixel
		d := (*uint32)((unsafe.Pointer)(&add[0]))

		AddUint8x4((*uint32)(unsafe.Pointer(v)), d) //call asm
	}

	stop := false
	nextSave := Config.SaveStart
	canSave := make([]chan uint64, Config.TreeCount)
	resumeSave := make([]chan uint64, Config.TreeCount)
	for ii := range canSave {
		canSave[ii] = make(chan uint64)
		resumeSave[ii] = make(chan uint64)
	}

	fmt.Println("Enter to exit")
	for ii := uint(0); ii < Config.TreeCount; ii++ {
		id := ii
		go func() {
			r := rand.New(rand.NewSource(rand.Int63())) //new RNG for each 'thread' so sync doesn't produce different results on same seed
			start := r.Intn(Config.Width)               //root point
			basehue := r.Float64() * 6
			currentCount := uint64(0)
			for {
				currentCount++
				if currentCount > nextSave { // time to save
					canSave[id] <- currentCount
					nextSave = <-resumeSave[id]
				}
				//New line
				x := float64(start) + (Gaussian(r) * Config.StartDiv * float64(Config.Width/100))
				y := float64(-.0001)
				ang := float64(math.Pi / 2)
				if Config.Unrooted {
					x = r.Float64() * float64(Config.Width)
					y = r.Float64() * float64(Config.Height)
					ang = r.Float64() * math.Pi * 2
				}
				age := r.Intn(Config.Height / 50)
				hue := basehue + r.Float64()*Config.RootColorFactor
				for hue > 6 {
					hue -= 6
				}
				for {
					x += math.Cos(ang) // velocity is always 1
					y += math.Sin(ang)
					//Out of bounds || random stop
					if r.Intn((Config.Height/100)*Config.LengthFactor) == 0 || x < 0 || x > float64(Config.Width) || y < 0 || y > float64(Config.Height) {
						break
					}
					//mutate
					ang += (r.Float64() - .5) / Config.StraightFactor
					age += r.Intn(2)
					lightness := ((r.Float64() * 2) + ((Config.AgeFactor * float64(age)) / float64(Config.Height))) / 255
					add := []int8{0, 0, 0, 0}
					{ // HSL to RGBA (sat is 1)
						cf := 1 - math.Abs((2*lightness)-1)
						xf := cf * (1 - math.Abs(math.Mod(hue, 2)-1))
						mf := lightness - (cf / 2)
						c := int8((mf + cf) * 256)
						x := int8((mf + xf) * 256)
						m := int8(mf * 256)
						h := int8(hue)
						switch h {
						case 0:
							add = []int8{c, x, m, 0}
						case 1:
							add = []int8{x, c, m, 0}
						case 2:
							add = []int8{m, c, x, 0}
						case 3:
							add = []int8{m, x, c, 0}
						case 4:
							add = []int8{x, m, c, 0}
						case 5:
							add = []int8{c, m, x, 0}
						default:
							add = []int8{0, 0, 0, 0}
							//fmt.Printf("%v %v", hue, lightness)
						}
					}
					//Draw it
					Add(uintptr(x), uintptr(y), add)
				}
			}
		}()
	}

	go func() {
		os.Stdin.Read(make([]byte, 1))
		stop = true
	}()

	go func() {
		ci := image.NewRGBA(bound)
		for {
			for _, v := range canSave[1:] {
				<-v
			}
			currentCount := <-canSave[0]
			copy(ci.Pix, i.Pix)
			nextSave := uint64(float64(currentCount) * Config.SaveInterval)
			fmt.Printf("Next save at: %d\n", nextSave)
			for i := range resumeSave {
				resumeSave[i] <- nextSave
			}
			fmt.Printf("Saving")
			Save(ci, currentCount)
			if stop {
				fmt.Println("; Done; Exiting")
				os.Exit(0)
			}
			fmt.Println("; Done")
		}
	}()

	<-make(chan bool)
}
