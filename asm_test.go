package main

import (
	"fmt"
	"testing"
)

func Test8x4(t *testing.T) {
	a := uint32(0x01020304)
	b := uint32(0x04050607)
	AddUint8x4(&a, &b)
	if b != 0x0507090B || a != b {
		fmt.Printf("a: %X \n", a)
		fmt.Printf("b: %X \n", b)
		t.Fail()
	}
}

func Test8x4Overflow(t *testing.T) {
	a := uint32(0x00990099)
	b := uint32(0x00990099)
	AddUint8x4(&a, &b)
	if b != 0x00FF00FF || a != b {
		fmt.Printf("a: %X \n", a)
		fmt.Printf("b: %X \n", b)
		t.Fail()
	}
}
